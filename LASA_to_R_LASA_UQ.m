% A Matlab script to augment the LASA dataset with Riemannian (unit quaternion (UQ)) motion profiles.
%
% The implemented procedure described in Saveriano et al., Learning Stable Robotic Skills on Riemannian Manifolds, 2021. 
%
% 
% Author: Matteo Saveriano
%
% Copyright (c) 2021 Matteo Saveriano, Dept. of Computer Science and 
% Digital Science Center, University of Innsbruck, 6020, Innsbruck,
% Austria, https://iis.uibk.ac.at
%

clear;
close all;

addpath(genpath('libs'))

modelPath = 'LASA_dataset/';

%% Preprocess demonstrations
% Define quaternion goal as [1, 0, 0, 0]
q_goal.s = 1;
q_goal.v = [0; 0; 0];

dt = 0.003; % Set an average sampling time
scale_ = 100;
for i=1:30
    % Load demonstrations
    [demos, ~, name]   = load_LASA_models(modelPath, i);
    
    % Stack all demos into a matrix
    demoMat = [];
    rangeUQ = [];
    for demoIt=1:length(demos)
        demoMat = [demoMat; demos{demoIt}.pos];
    end
    
    demoUQ = [];
    idx_ = [1:3; 5:7; 9:11; [13,14,1]];
    for demoUQIt=1:size(idx_,1)
        % Take 1 + 1/2 demonstrations to obtain a 3D motion, skip the rest
        demoUQ{demoUQIt}.tsPos = demoMat(idx_(demoUQIt,:),:);
        % Transform tsPos to meters to have reasonable TS vectors
        demoUQ{demoUQIt}.tsPos = demoUQ{demoUQIt}.tsPos ./ scale_;
        
        % Compute TS veloctiy
        demoUQ{demoUQIt}.tsVel = [diff(demoUQ{demoUQIt}.tsPos,[],2)./dt zeros(3,1)];
        
        % Store the sampling time
        demoUQ{demoUQIt}.dt = dt;  
        
        % Compute Unit Quaternion tajectory
        for tt=1:size(demoUQ{demoUQIt}.tsPos, 2)
            tmp = quat_exp(demoUQ{demoUQIt}.tsPos(:,tt));
            demoUQ{demoUQIt}.quat(:,tt) = quat2array(quat_mult(tmp, q_goal));
        end
    end
    
    filename = ['R_LASA_UQ/' name '_UQ.mat'];
    save(filename, 'demoUQ')
end
