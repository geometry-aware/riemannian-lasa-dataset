# Riemannian Lasa (R-LASA) Dataset
A dataset containing Riemannian (unit quaternion (UQ) and symmetric and positive definite (SPD) matrix) motion profiles.

The R-LASA dataset is obtained with the procedure described in [(Saveriano et al., 2023)](https://www.sciencedirect.com/science/article/pii/S0921889023001495) to augment an Euclidean dataset with Riemannian motions.

## Script description
- `LASA_to_R_LASA_UQ.m`: a script to augment the LASA Handwritten dataset with Riemannian (unit quaternion (UQ)) motion profiles.
- `LASA_to_R_LASA_SPD.m`: a script to augment the LASA Handwritten dataset with Riemannian (symmetirc and positive definite (SPD) matrices) motion profiles.

## Software Requirements
The code is developed and tested under `Ubuntu 18.04` and `Matlab2019b`.

## References
Please acknowledge the authors in any academic publication that used this dataset and/or the provided code.
```
@article{Saveriano2023Learning,
author = {Saveriano, Matteo and Abu-Dakka, Fares J. and Kyrki, Ville},
title = {Learning stable robotic skills on Riemannian manifolds},
journal = {Robotics and Autonomous Systems},
volume = {169},
pages = {104510},
year = {2023}
}

@article{Wang2022Learning,
  author={Wang, Weitao and Saveriano, Matteo and Abu-Dakka, Fares J.},
  title={Learning Deep Robotic Skills on Riemannian Manifolds}, 
  journal={IEEE Access},
  volume={10},
  pages={114143--114152},
  year={2022}
}

```

## Third-party material
Third-party code and dataset have been included in this repository for convenience.

- *LASA Handwritten dataset*: please acknowledge the authors in any academic publications that have made use of the LASA HandWritten dataset by citing: *S. M. Khansari-Zadeh and A. Billard, "Learning Stable Non-Linear Dynamical Systems with Gaussian Mixture Models", IEEE Transaction on Robotics, 2011*.

## Note
This source code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.
